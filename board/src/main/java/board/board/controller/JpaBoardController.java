package board.board.controller;

import java.io.File;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import board.board.entity.BoardEntity;
import board.board.entity.BoardFileEntity;
import board.board.service.JpaBoardService;

@Controller
public class JpaBoardController {

	@Autowired
	private JpaBoardService jpaBoardService;
	
	@GetMapping(value="/jpa/board")
	public ModelAndView openBoardList() throws Exception{
		ModelAndView mv = new ModelAndView("/board/jpaBoardList");
		
		List<BoardEntity> list = jpaBoardService.selectBoardList(); //게시판목록을 가져오는 서비스
		mv.addObject("list", list);
		
		return mv;
	}
	
	@GetMapping(value="/jpa/board/write")
	public String openBoardWrite() throws Exception{
		return "/board/jpaBoardWrite";
	}
	
	@PostMapping(value="/jpa/board/write")
	public String writeBoard(BoardEntity board, MultipartHttpServletRequest multipartHttpServletRequest) throws Exception{
		jpaBoardService.saveBoard(board, multipartHttpServletRequest);
		return "redirect:/jpa/board";
	}
	
	@GetMapping(value="/jpa/board/{boardIdx}")
	public ModelAndView openBoardDetail(@PathVariable("boardIdx") int boardIdx) throws Exception{
		ModelAndView mv = new ModelAndView("/board/jpaBoardDetail");
		
		BoardEntity board = jpaBoardService.selectBoardDetail(boardIdx);
		mv.addObject("board", board);
		
		return mv;
	}
	
	@PutMapping(value="/jpa/board/{boardIdx}")
	public String updateBoard(BoardEntity board, @PathVariable("boardIdx") int boardIdx) throws Exception{
		BoardEntity boardEntity = jpaBoardService.selectBoardDetail(boardIdx); 
		boardEntity.setTitle(board.getTitle());
		boardEntity.setContents(board.getContents());
		boardEntity.setUpdaterId("admin");
		boardEntity.setUpdatedDatetime(LocalDateTime.now());
		
		jpaBoardService.saveBoard(boardEntity, null);
		return "redirect:/jpa/board";
	}
	
	@DeleteMapping(value="/jpa/board/{boardIdx}")
	public String deleteBoard(@PathVariable("boardIdx") int boardIdx) throws Exception{
		jpaBoardService.deleteBoard(boardIdx);
		return "redirect:/jpa/board";
	}
	
	@GetMapping(value="/jpa/board/file")
	public void downloadBoardFile(@RequestParam int idx, @RequestParam int boardIdx, HttpServletResponse response) throws Exception{
		BoardFileEntity file = jpaBoardService.selectBoardFileInformation(boardIdx, idx);
			
			byte[] files = FileUtils.readFileToByteArray(new File(file.getStoredFilePath()));
			
			response.setContentType("application/octet-stream");
			response.setContentLength(files.length);
			response.setHeader("Content-Disposition", "attachment; fileName=\"" + 
				URLEncoder.encode(file.getOriginalFileName(), "UTF-8") + "\";");
			response.setHeader("Content-Transfer-Encoding", "binary");
			
			response.getOutputStream().write(files);
			response.getOutputStream().flush();
			response.getOutputStream().close();
			
			
		}
		
	}

